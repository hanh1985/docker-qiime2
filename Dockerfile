ARG	IMAGE=ubuntu 
ARG	IMAGE_TAG=16.04
FROM	${IMAGE}:${IMAGE_TAG}

#ENV ARGRUMENT
ENV	LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV 	HOME=/root
ENV	PATH=/opt/conda/bin:$PATH

#install depend command
RUN	apt-get update --fix-missing && \
	apt-get install -y wget bzip2  ca-certificates unzip \
	libglib2.0-0 libxext6 libsm6 libxrender1 git vim tmux curl \
	software-properties-common build-essential \
	libx11-dev libpng-dev


#install Miniconda3
RUN 	echo 'export path="/opt/conda/bin:$PATH"' >/etc/profile.d/conda.sh && \
	wget --quiet https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O $HOME/conda.sh && \
	chmod 777 $HOME/conda.sh && \
	/bin/bash    $HOME/conda.sh -b -p /opt/conda && \
	export path="/opt/conda/bin:$PATH"  && \
	rm $HOME/conda.sh


#Update Conda 
RUN	conda update conda && \
	conda install wget && \
	conda install anaconda-client
 
#install Qiime2
RUN	wget https://data.qiime2.org/distro/core/qiime2-2018.4-py35-linux-conda.yml -O $HOME/qiime2.yml && \
	export path="/opt/conda/bin:$PATH" && \ 
	conda env create -n qiime2-2018.4 --file $HOME/qiime2.yml && \ 
	rm ~/qiime2.yml 



#Define working directory
WORKDIR	/data


#Clean Temp_file
RUN	rm -rf /var/lib/apt/lists/* && \
	rm -rf /tmp/*

#Define Default command.
CMD ["bash"]
